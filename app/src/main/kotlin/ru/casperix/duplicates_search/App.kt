package ru.casperix.duplicates_search

import ru.casperix.duplicates_search.impl.DuplicatesSearch.scanPath
import kotlin.io.path.Path


fun main(args: Array<String>) {
    val path = args.getOrNull(0) ?: run {
        println("<invalid path>")
        return
    }

    val groups = scanPath(Path(path))


    val output = groups.map {
        it.joinToString(";")
    }.joinToString("\n")

    println(output)
}
