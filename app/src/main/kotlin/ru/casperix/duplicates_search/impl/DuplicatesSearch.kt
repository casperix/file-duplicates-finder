package ru.casperix.duplicates_search.impl

import kotlinx.coroutines.*
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.security.MessageDigest
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.Volatile
import kotlin.io.path.Path
import kotlin.io.path.absolute
import kotlin.math.max
import kotlin.math.min


object DuplicatesSearch {
    private val logger = LoggerFactory.getLogger(javaClass)

    private const val MIN_BLOCK_SIZE = 1024L
    private const val MAX_BLOCK_SIZE = 128L * 1024L * 1024L
    private val HALF_THREADS = max(1, Runtime.getRuntime().availableProcessors() / 2)

    enum class ScanPhase {
        INIT,
        COLLECT,
        DIV_BY_SIZE,
        DIV_BY_HASH,
        DIV_BY_CONTENT,
        COMPLETE,
    }

    class ScanState(@Volatile var phase: ScanPhase) {
        @Volatile
        var progress = 0f//Не участвует в аглоритме. Ошибка не критична
    }

    data class ScanContext(val scope: CoroutineScope, val config: DuplicatesSearchConfig, val state: ScanState)

    data class FileEntry(val path: Path, val size: Long)

    fun scanPath(path: String, config: DuplicatesSearchConfig = DuplicatesSearchConfig()): List<List<String>> {
        return scanPath(Path(path), config).map { fileList ->
            fileList.map { file ->
                file.toString()
            }
        }
    }

    fun scanPath(path: Path, config: DuplicatesSearchConfig = DuplicatesSearchConfig()): List<List<Path>> {
        return runBlocking(Dispatchers.IO) {
            scanPathSuspend(this, path, config)
        }
    }

    private suspend fun scanPathSuspend(
        scope: CoroutineScope,
        path: Path,
        config: DuplicatesSearchConfig = DuplicatesSearchConfig()
    ): List<List<Path>> = ScanContext(scope, config, ScanState(ScanPhase.INIT)).run {
        val printStateJob = stateLogJob()

        val files = collectPhase(path)
        val sizeGroupMap = divideBySizePhase(files)
        val bySizeHashList = divideByHashPhase(sizeGroupMap)
        val byContentList = divideByContentPhase(bySizeHashList)

        state.phase = ScanPhase.COMPLETE
        printStateJob.cancel()
        return byContentList
    }

    private suspend fun ScanContext.collectPhase(path: Path): List<Path> {
        state.phase = ScanPhase.COLLECT
        state.progress = 0.1f

        val originalFiles = getAllFiles(path)
        logger.info("scan path: ${path.absolute()}")
        logger.info("original files: ${originalFiles.size}")

        val filteredFiles = if (config.strongCheckAccess) {
            getAvailableFiles(originalFiles)
        } else originalFiles
        logger.info("filtered files: ${filteredFiles.size}")
        return filteredFiles
    }

    private suspend fun ScanContext.divideBySizePhase(files: List<Path>): Map<Long, List<Path>> {
        state.phase = ScanPhase.DIV_BY_SIZE
        state.progress = 0.2f

        val sizeGroupMap = buildSizeMap(files)
        logger.info("summary size: ${getSummarySize(sizeGroupMap) / 1024L / 1024L}Mb")
        return sizeGroupMap
    }

    private suspend fun ScanContext.divideByHashPhase(bySizeGroups: Map<Long, List<Path>>): List<List<FileEntry>> {
        state.phase = ScanPhase.DIV_BY_HASH

        val chunkList = bySizeGroups.entries.chunked(HALF_THREADS)

        val bySizeHashGroups = chunkList.map { chunk ->
            scope.async {
                chunk.flatMap { (size, sameSizeItems) ->
                    divideByHash(size, sameSizeItems)
                }
            }.apply {
                invokeOnCompletion {
                    state.progress = 0.3f + 0.4f / chunkList.size
                }
            }
        }.awaitAll().flatten()


        var allDuplicates = 0
        var maxGroupSize = 0
        bySizeHashGroups.forEach { hashBasketMap ->
            maxGroupSize = max(maxGroupSize, hashBasketMap.size)
            if (hashBasketMap.size > 1) {
                allDuplicates += hashBasketMap.size
            }
        }
        logger.info("equal hash: $allDuplicates")
        logger.info("max group: $maxGroupSize")
        return bySizeHashGroups
    }

    private suspend fun ScanContext.divideByContentPhase(bySizeHashGroups: List<List<FileEntry>>): List<List<Path>> {
        state.phase = ScanPhase.DIV_BY_CONTENT

        val chunkList = bySizeHashGroups.chunked(HALF_THREADS)

        val output = chunkList.map { chunk ->
            scope.async {
                chunk.flatMap {
                    ContentComparator.divideByContent(scope, it)
                }
            }.apply {
                invokeOnCompletion {
                    state.progress = 0.7f + 0.3f / chunkList.size
                }
            }
        }.awaitAll().flatten()

        return output
    }

    private fun ScanContext.stateLogJob(): Job {
        return scope.launch {
            while (true) {
                delay(2000L)
                val progressFormatted = String.format("%.2f%%", state.progress * 100f)
                logger.info("progress: $progressFormatted (${state.phase.name.lowercase()})")
            }
        }
    }

    private suspend fun ScanContext.getAvailableFiles(originalFiles: List<Path>): List<Path> {
        return originalFiles.map { path ->
            scope.async {
                Pair(path, path.toFile().canReadWithCheck())
            }
        }.awaitAll().mapNotNull { (path, allow) ->
            if (allow) {
                path
            } else {
                null
            }
        }
    }

    private fun <T> fileOperation(path: Path, operation: () -> T): T {
        try {
            return operation()
        } catch (e: Throwable) {
            throw FileOperationError(path, e)
        }
    }

    private fun ScanContext.buildSizeMap(files: List<Path>): Map<Long, List<Path>> {
        if (files.isEmpty()) return emptyMap()
        if (!config.useFileSizeAttribute) {
            //  TODO: use null if size unknown
            return mapOf(Pair(0, files))
        }

        val basketBySizeMap = mutableMapOf<Long, MutableList<Path>>()
        files.forEach {
            fileOperation(it) {
                val size = Files.size(it)
                val list = basketBySizeMap.getOrPut(size) { mutableListOf() }
                list.add(it)
            }
        }

        return basketBySizeMap
    }

    private fun getSummarySize(basketBySizeMap: Map<Long, List<Path>>): Long {
        var summarySize = 0L
        basketBySizeMap.entries.forEach { (size, duplicates) ->
            summarySize += size * duplicates.size
        }
        return summarySize
    }

    private suspend fun ScanContext.divideByHash(size: Long, files: List<Path>): List<List<FileEntry>> {
        if (files.isEmpty()) {
            return emptyList()
        }

        if (files.size == 1) {
            //  Нет нужды считать хэш: один элемент -> один список
            return listOf(listOf(FileEntry(files.first(), size)))
        }

        val pathAndHash = files.map { path ->
            scope.async {
                fileOperation(path) {
                    val hash = calculateHash( path, size, getOptimalBlockSize(files.size))
                    Pair(path, hash)
                }
            }
        }.awaitAll()

        val hashMap = mutableMapOf<ByteArrayKey, MutableList<Path>>()
        pathAndHash.forEach { (path, hash) ->
            hashMap.getOrPut(ByteArrayKey(hash)) { mutableListOf() }.add(path)
        }

        return hashMap.values.toList().map {
            it.map {
                FileEntry(it, size)
            }
        }
    }


    private fun ScanContext.calculateHash(path: Path, fileSize: Long, blockSize: Long): ByteArray {
        if (config.algorithm == null) {
            //  TODO: ignore all hash-phase
            return ByteArray(0)
        }

        logger.debug("Calculate hash for: {}", path.absolute())
        val hashBuilder = MessageDigest.getInstance(config.algorithm.rawName)

        val file = path.toFile()
        val input = file.inputStream()
        var remainBytes = fileSize
        while (true/*remainBytes > 0L*/) {
            val readLength = min(remainBytes, min(blockSize, input.available().toLong()))
            if (readLength == 0L) break

            val bytes = input.readNBytes(readLength.toInt())

            remainBytes -= readLength
            hashBuilder.update(bytes)
        }
        if (remainBytes != 0L && config.useFileSizeAttribute) {
            //TODO: ignore file-size-attribute and recalculate
            throw Exception("Expected file size: $fileSize. But actual: ${fileSize - remainBytes}")
        }
        return hashBuilder.digest()
    }

    /**
     *  Некоторые файлы показаны как читаемые, но фактическое чтение выдает ошибку.
     *  Поэтому делаем пробную попытку чтения
     *
     *  Windows:
     *  https://bugs.java.com/bugdatabase/view_bug?bug_id=6203387
     */
    private fun File.canReadWithCheck(): Boolean {
        return try {
            if (!canRead()) return false

            inputStream().use {
                if (it.available() >= 1) {
                    it.readNBytes(1)
                }
            }

            true
        } catch (e: Throwable) {
            false
        }
    }

    fun getOptimalBlockSize(recordAmount: Int): Long = max(MIN_BLOCK_SIZE, MAX_BLOCK_SIZE / recordAmount)

    fun getAllFiles(path: Path): List<Path> {
        val file = path.toFile()
        return file.walk().toList().mapNotNull {
            if (it.isFile) {
                it.toPath()
            } else null
        }
    }
}

