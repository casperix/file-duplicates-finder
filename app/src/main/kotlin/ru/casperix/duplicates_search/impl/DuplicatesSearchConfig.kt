package ru.casperix.duplicates_search.impl

data class DuplicatesSearchConfig(
    val useFileSizeAttribute:Boolean = true,
    val strongCheckAccess:Boolean = true,
    val ignoreProhibitedFiles: Boolean = true,
    val ignoreLockedFiles: Boolean = true,
    val algorithm: HashAlgorithm? = HashAlgorithm.SHA512,
)