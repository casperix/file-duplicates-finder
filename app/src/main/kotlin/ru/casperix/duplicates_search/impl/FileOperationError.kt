package ru.casperix.duplicates_search.impl

import java.nio.file.Path

data class FileOperationError(val path: Path, override val cause:Throwable) : Exception(cause)