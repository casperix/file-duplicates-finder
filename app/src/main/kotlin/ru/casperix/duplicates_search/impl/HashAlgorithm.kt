package ru.casperix.duplicates_search.impl

enum class HashAlgorithm(val rawName: String) {
    MD5("MD5"),
    SHA1("SHA-1"),
    SHA256("SHA-256"),
    SHA512("SHA-512"),
}