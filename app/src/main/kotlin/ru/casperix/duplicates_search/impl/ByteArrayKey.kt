package ru.casperix.duplicates_search.impl

data class ByteArrayKey(val byteArray: ByteArray) {
    private val hashCodeCache = byteArray.contentHashCode()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ByteArrayKey

        return byteArray.contentEquals(other.byteArray)
    }

    override fun hashCode(): Int {
        return hashCodeCache
    }
}