package ru.casperix.duplicates_search.impl

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import ru.casperix.duplicates_search.impl.DuplicatesSearch.getOptimalBlockSize
import java.io.InputStream
import java.nio.file.Path
import kotlin.math.min

object ContentComparator {

    class FileContentEntry(val path: Path, val stream: InputStream)

    suspend fun divideByContent(scope: CoroutineScope, files: List<DuplicatesSearch.FileEntry>): List<List<Path>> {
        //if empty or one -- not need any calculate
        if (files.isEmpty()) return emptyList()
        if (files.size == 1) return listOf(listOf(files.first().path))

        /**
         * В принципе мы можем отдельно обработать "мелкие" файлы -- считать их целиком и сравнить.
         *
         * В общем же случае мы не можем рассчитывать что все файлы-кандидаты поместится в ОЗУ:
         * Мы считываем параллельно все файлы кандидаты блоками (и сравниваем блоки)
         *
         * Нам нужно сравнивать каждый блок с каждым "o(n)".
         * Однако на данном этапе с большой вероятностью мы работаем именно с дубликатами (совпадает хэш и размер)
         */

        val duplicateExcludeMap = MutableList(files.size) { mutableListOf<Int>() }
        val blockSize = getOptimalBlockSize(files.size)

        val entries = mutableListOf<FileContentEntry>()
        try {
            //  if any file exception, then correctly close
            files.forEach {
                entries += FileContentEntry(it.path, it.path.toFile().inputStream())
            }

            while (true) {
                val available = entries.mapIndexed { index, entry ->
                    entry.stream.available()
                }.min()

                //TODO: generate exception if not all stream empty
                if (available == 0) {
                    break
                }

                val readLength = min(blockSize.toInt(), available)

                val buffers = entries.map {
                    scope.async {
                        it.stream.readNBytes(readLength)
                    }
                }.awaitAll()

                buffers.flatMapIndexed { indexA, bufferA ->
                    (0 until indexA).map { indexB ->
                        val isExclude = duplicateExcludeMap[indexA].contains(indexB)
                        if (!isExclude) {
                            val bufferB = buffers[indexB]
                            if (!bufferA.contentEquals(bufferB)) {
                                duplicateExcludeMap[indexA].add(indexB)
                                duplicateExcludeMap[indexB].add(indexA)
                            }
                        }
                    }
                }
            }

        } finally {
            entries.forEach { it.stream.close() }
        }

        val duplicateMap = mutableMapOf<Path, MutableList<Path>>()
        files.forEachIndexed { indexA, fileA ->
            files.forEachIndexed { indexB, fileB ->
                val isExclude = duplicateExcludeMap[indexA].contains(indexB)
                if (!isExclude) {
                    duplicateMap.getOrPut(fileA.path) { mutableListOf() }.add(fileB.path)
                }
            }
        }

        /**
         *  If fileA==fileB then fileA==fileB -- need remove duplicate records
         */
        val output = files.map {
            (duplicateMap[it.path].orEmpty() + listOf(it.path)).sorted().toSet().toList()
        }.toSet().toList()
        return output
    }
}