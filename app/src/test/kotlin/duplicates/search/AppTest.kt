package duplicates.search

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.slf4j.LoggerFactory
import ru.casperix.duplicates_search.impl.DuplicatesSearch
import ru.casperix.duplicates_search.impl.DuplicatesSearchConfig
import java.io.File
import java.nio.file.Files
import kotlin.io.path.Path
import kotlin.random.Random
import kotlin.random.nextInt
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.time.measureTime
import kotlin.time.measureTimedValue

class AppTest {


    data class FileGeneratorConfig(
        val amount: Int = 20,
        val duplicateChance: Float = 0.2f,
        val sameFileAmount: IntRange = 1..100,
        val sizeRange: IntRange = 100..1000_000

    )

    @Test
    fun simpleTest() {
        customTest("Simple", FileGeneratorConfig(), DuplicatesSearchConfig(strongCheckAccess = false))
    }

    @Test
    fun swarmFilesTest() {
        customTest("SwarmFiles", FileGeneratorConfig(10_000, 0.0005f, 1..2, 2_000..3_000), DuplicatesSearchConfig(strongCheckAccess = false))
    }

    @Test
    fun manyDuplicateTest() {
        customTest("ManyDuplicates", FileGeneratorConfig(100, 0.1f, 1..100, 10_000..1000_000), DuplicatesSearchConfig(strongCheckAccess = false))
    }

    @Test
    fun bigFilesTest() {
        customTest("BigFiles", FileGeneratorConfig(10, 0.5f, 1..10, 10_000_000..100_000_000), DuplicatesSearchConfig(strongCheckAccess = false))
    }

    @Test
    fun scanSimple() {
        scanAndLog("src/test/resources", DuplicatesSearchConfig(strongCheckAccess = false))
    }

    /**
     * Files can equal hash, but not equal content. Test with empty hash
     */
    @Test
    fun scanNoHash() {
        scanAndLog("src/test/resources", DuplicatesSearchConfig(strongCheckAccess = false, algorithm = null))
    }

    /**
     * File size in attributes can not equal real content size. Test without use file-size-attribute
     */
    @Test
    fun scanNoSize() {
        scanAndLog("src/test/resources", DuplicatesSearchConfig(useFileSizeAttribute = false, strongCheckAccess = false, algorithm = null))
    }

    @Test
    @Disabled
    fun scanVideo() {
        //TODO: Now work only for concrete environment
        scanAndLog("C:/Video", DuplicatesSearchConfig(strongCheckAccess = false))
    }

    @Test
    @Disabled
    fun scanSystem() {
        //TODO: Now work only for concrete environment
        scanAndLog("C:/Windows", DuplicatesSearchConfig(strongCheckAccess = true))
    }

    @Test
    @Disabled
    fun scanProjects() {
        //TODO: Now work only for concrete environment
        scanAndLog("C:/Users/MJ/Projects/", DuplicatesSearchConfig(strongCheckAccess = true))
    }


    @Test
    fun collectFileTest() {
        val time = measureTime {
            DuplicatesSearch.getAllFiles(Path("C:/Users/MJ/Projects/"))
        }
        logger.info("collect for: ${time.inWholeMilliseconds}ms")
    }

    private fun customTest(name: String, generatorConfig: FileGeneratorConfig, duplicatesConfig:DuplicatesSearchConfig) {
        logger.info("")
        logger.info("Test: $name")

        val testPath = commonTestsPath + File.separator + name + File.separator

        val (generatedOutput, generatedTime) = measureTimedValue {
            sortOutput(generateTestFiles(testPath, generatorConfig))
        }
        logger.info("Generated test files for: ${generatedTime.inWholeMilliseconds}ms")

        val (testOutput, testTime) = measureTimedValue {
            sortOutput(DuplicatesSearch.scanPath(testPath, duplicatesConfig))
        }
        logger.info("Duplicates search for: ${testTime.inWholeMilliseconds}ms")

        assertContentEquals(generatedOutput, testOutput)
    }

    private fun scanAndLog(testPath: String, config: DuplicatesSearchConfig = DuplicatesSearchConfig()) {
        val (testOutput, testTime) = measureTimedValue {
            DuplicatesSearch.scanPath(testPath, config)
        }
        logger.info("Duplicates search for: ${testTime.inWholeMilliseconds}ms")

        val duplicateAmount = testOutput.map { if (it.size <= 1) 0 else it.size }.reduceOrNull { a, b -> a + b } ?: 0
        logger.info("Duplicates: $duplicateAmount")
    }

    @OptIn(ExperimentalStdlibApi::class)
    fun generateTestFiles(testPath: String, config: FileGeneratorConfig = FileGeneratorConfig()): List<List<String>> = config.run {
        if (!File(commonTestsPath).exists()) {
            Files.createDirectory(Path(commonTestsPath))
        }

        if (!File(testPath).exists()) {
            Files.createDirectory(Path(testPath))
            Files.createDirectory(Path(testPath + File.separator + "original"))
            Files.createDirectory(Path(testPath + File.separator + "duplicate"))
        }


        return (0 until amount).map { index ->
            val random = Random(index)
            val length = random.nextInt(sizeRange)
            val content = random.nextBytes(length)

            val originalName = random.nextBytes(8).toHexString()
            val originalPath = "${testPath}original${File.separator}$originalName"
            val hasDuplicates = random.nextFloat() > (1f - duplicateChance)
            val sameContentFileList = mutableListOf(originalPath)

            if (hasDuplicates) {
                val duplicateAmount = random.nextInt(sameFileAmount)
                val duplicateList = (0 until duplicateAmount).map {
                    val duplicateName = random.nextBytes(8).toHexString()
                    val duplicatePath = "${testPath}duplicate${File.separator}$duplicateName"
                    duplicatePath
                }
                sameContentFileList += duplicateList
            }

            sameContentFileList.forEach {
                File(it).writeBytes(content)
            }
            sameContentFileList
        }
    }

    private fun sortOutput(output: List<List<String>>): List<List<String>> {
        return output.map {
            it.sorted()
        }.sortedBy { row ->
            row.joinToString(";")
        }
    }

    companion object {
        private val commonTestsPath = "test_files"
        val logger = LoggerFactory.getLogger(AppTest.javaClass)

        @JvmStatic
        @AfterAll
        fun clean() {
            File(commonTestsPath).deleteRecursively()
        }

        @JvmStatic
        @BeforeAll
        fun printInfo() {
            val processors = Runtime.getRuntime().availableProcessors()
            val memory = Runtime.getRuntime().maxMemory()

            logger.info("Processors: $processors")
            logger.info("Memory: ${memory / 1024 / 1024} Mb")
        }
    }


}
