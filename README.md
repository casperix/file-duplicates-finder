## поиск файлов-дупликатов

Тесты:

`AppTest`

Собрать версию:

`gradle jar`
- build/libs/duplicates-finder-XXX.jar


Запустить версию (требуется java-20):

`java -jar .\duplicates-finder-0.9.4.jar "Some/Path"`

- дупликаты на одной строчке через `;`
- уникальные файлы занимают одну строчку